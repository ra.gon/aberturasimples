import * as C from './styles';

export const Header = () => {
    return (
        <C.Container>
            <h1>Tela de Pagamento</h1>
        </C.Container>
    );
}