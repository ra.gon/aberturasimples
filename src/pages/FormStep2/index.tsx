import { useHistory, Link } from 'react-router-dom';
import * as C from './styles';
import { useForm, FormActions } from '../../contexts/FormContext';
import { Theme } from '../../components/Theme';
import { ChangeEvent, useEffect } from 'react';
import { SelectOption } from '../../components/SelectOption';

export const FormStep2 = () => {
    const history = useHistory();
    const { state, dispatch } = useForm();

    useEffect(() => {
        if(state.name === '') {
            history.push('/');
        } else {
            dispatch({
                type: FormActions.setCurrentStep,
                payload: 2
            });
        }
    }, []);

    const handleNextStep = () => {
        if(state.name !== '') {
            history.push('/step3');
        } else {
            alert("Preencha os dados.");
        }
    }

    const setPayment = (payment: number) => {
        dispatch({
            type: FormActions.setPayment,
            payload: payment
        });
    }

    return (
        <Theme>
            <C.Container>
                <p>Passo 2/3</p>
                <h1>{state.name}, qual a forma de pagamento?</h1>
                <p>Escolha a opção que irá realizar o pagamento.</p>

                <hr/>

                <SelectOption
                    title="Cartão de crédito"
                    description="Realizar pagamento com cartão de crédito"
                    icon="🥳"
                    selected={state.payment === 0}
                    onClick={()=>setPayment(0)}
                />

                <SelectOption
                    title="Boleto"
                    description="Realizar pagamento com boleto"
                    icon="😎"
                    selected={state.payment === 1}
                    onClick={()=>setPayment(1)}
                />

                <Link to="/" className="backButton">Voltar</Link>
                <button onClick={handleNextStep}>Próximo</button>
            </C.Container>
        </Theme>
    );
}

function setPayment(arg0: number): void {
    throw new Error('Function not implemented.');
}
