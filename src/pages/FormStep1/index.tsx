import { useHistory } from 'react-router-dom';
import * as C from './styles';
import { useForm, FormActions } from '../../contexts/FormContext';
import { Theme } from '../../components/Theme';
import { ChangeEvent, useEffect } from 'react';

export const FormStep1 = () => {
    const history = useHistory();
    const { state, dispatch } = useForm();

    useEffect(() => {
        dispatch({
            type: FormActions.setCurrentStep,
            payload: 1
        });
    }, []);

    const handleNextStep = () => {
        if(state.name !== '' && state.email !== '' && state.confirmEmail !== '' && state.doc !== '') {
            history.push('/step2');
        } else {
            alert("Preencha os dados.");
        }
    }

    const handleNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        dispatch({
            type: FormActions.setName,
            payload: e.target.value
        });
    }

    const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
        dispatch({
            type: FormActions.setEmail,
            payload: e.target.value
        });
    }

    const handleConfirmEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
        dispatch({
            type: FormActions.setConfirmEmail,
            payload: e.target.value
        });
    }

    const handleDocChange = (e: ChangeEvent<HTMLInputElement>) => {
        dispatch({
            type: FormActions.setDoc,
            payload: e.target.value
        });
    }

    return (
        <Theme>
            <C.Container>
                <p>Passo 1/3</p>
                <h1>Preencha seus dados pessoais</h1>
                <p>Preencha os campos obrigatórios abaixo.</p>

                <hr/>

                <label>
                    SEU NOME COMPLETO
                    <input
                        type="text"
                        autoFocus
                        value={state.name}
                        onChange={handleNameChange}
                    />
                </label>

                <label>
                    SEU EMAIL
                    <input
                        type="text"
                        autoFocus
                        value={state.email}
                        onChange={handleEmailChange}
                    />
                </label>

                <label>
                    CONFIRME SEU EMAIL
                    <input
                        type="text"
                        autoFocus
                        value={state.confirmEmail}
                        onChange={handleConfirmEmailChange}
                    />
                </label>

                <label>
                    CPF/CNPJ
                    <input
                        type="text"
                        autoFocus
                        value={state.doc}
                        onChange={handleDocChange}
                    />
                </label>

                <button onClick={handleNextStep}>Próximo</button>
            </C.Container>
        </Theme>
    );
}